#!/usr/bin/python3
#
#    DevDetect - small utility to detect new devices on local network
#    Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
import socket
import sys
import json
import time
import logging
import os
from dev_detect.mac_addr_store import MacAddrStore

logger = logging.getLogger('devdetect')
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

SURICATA_SOCKET='/tmp/suricata/dev_detect.sock'

def main(argv = sys.argv):
	try:
		os.unlink(SURICATA_SOCKET) #remove stray socket
	except OSError:
		pass
	if len(argv) < 2:
		logger.error("usage: {} interface [interface] ...".format(argv[0]))
		return 1
	interfaces = argv[1:]
	suricata_sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
	suricata_sock.bind(SURICATA_SOCKET)
	mac_store = MacAddrStore(interfaces)
	while True:
		try:
			raw_data = suricata_sock.recv(2048).decode('utf-8', 'ignore')
			data = json.loads(raw_data)
			if data["event_type"] == "flow_start":
				mac_store.update(data["ether"]["src"])
		except ValueError:
			logger.warn("received malformed record (invalid JSON) from pakond: {}".format(raw_data))
		except KeyError as err:
			#We want to ignore missing keys "ether", "src" - they may be missing, but we don't want warn about that
			if err.args[0] != "ether" and err.args[0] != "src":
				logger.warn("received malformed report (missing expected key) from pakond: {}".format(raw_data))

if __name__ == "__main__":
	main()
