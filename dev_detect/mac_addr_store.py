#
#    DevDetect - small utility to detect new devices on local network
#    Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# -*- coding: utf-8 -*-

import time
import json
import os
import sys
import logging
import subprocess
import threading

logger = logging.getLogger('devdetect')

STORE_FILE = '/usr/share/pakon-dev-detect/known_macs'

class MacAddrStore():
	"""keeps MAC addresses as dictionary, MAC address is the key, timestamp is the value"""
	def __init__(self, interfaces):
		self.__load()
		self.save_interval = 30*60 #TODO: move to configuration
		self.mac_timeout = 60*60*24*30 #TODO: move to configuration
		self.__last_save = int(time.time())
		self.__interfaces = interfaces
		#add "*" to self.__interfaces
		#that way, if get_mac_iface.sh doesn't exists we treat that as ANY_IFACE and we always create notification
		#without that, if get_mac_iface.sh fails, the notification will never be created
		self.__interfaces.append("*")

	def update(self, mac):
		"""update MAC address (its timestamp).
		if it wasn't seen before, register it (also save file immdiatelly) and create notification for user
		timestamp in __known is updated if current timestamp is bigger
		 - during install we are adding local MAC addresses with timestamp 2^32-1, we don't want them to ever expire

		also saves the file, when save_interval passes
		"""
		if mac in self.__known:
			self.__known[mac] = max(int(time.time()), self.__known[mac])
		else:
			device = self.__lookup_interface(mac)
			if device not in self.__interfaces:
				return
			t = threading.Thread(target=self.__new_device, args=(mac,device,))
			t.daemon = True
			t.start()
			self.__known[mac] = int(time.time())
			self.save()

	def save(self):
		"""saves known MAC addresses to file
		to prevent file corruption, MAC addresses are at first written to temporary files which is then moved to correct location
		"""
		for mac in self.__known.keys():
			if self.__known[mac] < int(time.time()) - self.mac_timeout:
				logger.info("deleted MAC address %s - inactive since %d", mac, self.__known[mac])
				del self.__known[mac]
		tmpfile = STORE_FILE + '.tmp'
		with open(tmpfile, 'w') as f:
			json.dump(self.__known, f)
		os.rename(tmpfile, STORE_FILE)
		logger.debug("saved MAC addresses to file")
		self.__last_save = int(time.time())

	def __load(self):
		"""loads addresses (if they exist)"""
		try:
			with open(STORE_FILE) as f:
				self.__known = json.load(f)
			logger.debug("loaded known MAC adresses file")
		except (OSError, ValueError): #error reading file (file doesn't exist or is unreadable or can't parse JSON)
			self.__known = {} #initialize clean set
			logger.debug("failed to load known MAC adresses file, initializing clean list")

	def __new_device(self, mac, device):
		"""this method is called when new device is detected (called in thread)"""
		logger.info("discovered new device (MAC %s)", mac)
		#it might be beneficial to wait for a while before looking up hostname, because some devices tries some IPv6 connections before asking DHCP for IPv4 address
		time.sleep(5)
		dhcp_info = self.__lookup_dhcp_leases(mac)
		if "guest_turris" in device:
			text_cz = "Ve vaší síti pro hosty se objevilo nové zařízení (MAC adresa {}".format(mac)
			text_en = "New device appeared on your guest network (MAC address {}".format(mac)
		else:
			text_cz = "Ve vaší síti se objevilo nové zařízení (MAC adresa {}".format(mac)
			text_en = "New device appeared on your network (MAC address {}".format(mac)
		vendor_info=self.__get_mac_vendor(mac)
		if vendor_info:
			text_cz += ", výrobce {}".format(vendor_info)
			text_en += ", vendor {}".format(vendor_info)
		if dhcp_info:
			text_cz += ", hostname {}".format(dhcp_info)
			text_en += ", hostname {}".format(dhcp_info)
		text_cz += ")."
		text_en += ")."
		try:
			#UTF-8 encoding isn't default on blue turris, we need to encode it explicitly
			cmd = ["/usr/bin/create_notification", "-s", "news", text_cz, text_en]
			subprocess.call([arg.encode('utf-8') for arg in cmd])
		except OSError:
			logger.error("failed to create notification")

	def __lookup_dhcp_leases(self, mac):
		"""parse dhcp.leases, try to find mac
		file format: "timestamp(expiry) mac ip hostname clientID. Hostname or clientID mght be * (unknown).
		returns: string hostname - or "" if it's not found"""
		try:
			with open('/tmp/dhcp.leases') as f:
				for line in f:
					cols = line.split(' ')
					if mac.upper() == cols[1].upper():
						return cols[3] if cols[3]!="*" else ""
		except OSError:
			logger.warn("failed to get hostname from dhcp.leases")
		return ""

	def __get_mac_vendor(self, mac):
		"""gets MAC vendor (using ouidb)
		returns: string vendor - or "" if it's not found"""
		try:
			vendor_name = subprocess.check_output(['/usr/bin/ouidb', mac]).decode()
			if vendor_name:
				return vendor_name.rstrip() #remove trailing newline
		except OSError:
			logger.warn("failed to get MAC vendor (using ouidb)")
		return ""

	def __lookup_interface(self, mac):
		"""gets interface for this MAC address (from ip neigh)
		returns: string interface - or "" if it's not found (that might happen if address is local)"""
		try:
			interface = subprocess.check_output(['/usr/share/pakon-dev-detect/get_mac_iface.sh', mac])
			if interface:
				return interface.decode().rstrip() #remove trailing newline
			else:
				return ""
		except OSError:
			logger.warn("failed to get interface (using get_mac_iface.sh)")
		return "*"
