#!/usr/bin/python3
import os
from setuptools import setup

classifiers = ['Development Status :: 4 - Beta',
               'Operating System :: POSIX :: Linux',
               'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
               'Intended Audience :: Developers',
               'Programming Language :: Python :: 3',
               'Topic :: System :: Hardware']


setup(
    name = 'dev_detect',
    version = '1.2',
    license = 'GPLv2+',
    url = 'https://gitlab.labs.nic.cz/turris/devdetect',
    author = 'Martin Petracek',
    author_email = 'martin.petracek@nic.cz',
    description = 'A utility to detect new devices on local network',
    zip_safe = True,
    packages = ['dev_detect'],
    entry_points={
        'console_scripts': [
            'dev-detect = dev_detect.__main__:main'
        ]
    },
    classifiers=classifiers,
)
