#!/usr/bin/python3
#
#    DevDetect - small utility to detect new devices on local network
#    Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

from multiprocessing import Process
import socket
import time
import os
import sys
import threading
import dev_detect.__main__ as main
import dev_detect.mac_addr_store as mac_addr_store

REPORT_FLOW_WITHOUT_MAC = b'{"timestamp":"2017-07-24T12:56:58.236099+0200","flow_id":1249701575432771,"event_type":"flow_start","src_ip":"fe80:0000:0000:0000:0000:0000:0000:002c","dest_ip":"ff02:0000:0000:0000:0000:0000:0000:0002","proto":"IPv6-ICMP","icmp_type":131,"icmp_code":0}\n'
REPORT_FLOW_WITH_MAC = b'{"timestamp":"2017-07-24T12:57:45.400006+0200","flow_id":2164877504879238,"event_type":"flow_start","src_ip":"192.168.1.126","src_port":39592,"dest_ip":"192.168.123.123","dest_port":443,"proto":"TCP","ether":{"src":"d4:81:d7:00:01:02"},"in_dev":"br-lan"}\n'

def fake_suricata(sock_path):
	sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
	retry = 10
	time.sleep(1)
	while retry > 0:
		try:
			sock.connect(sock_path)
			break
		except:
			time.sleep(1)
			retry -= 1
	sock.send(REPORT_FLOW_WITHOUT_MAC)
	sock.send(REPORT_FLOW_WITH_MAC)
	sock.makefile().flush()
	time.sleep(1)
	sock.close()
	os.remove(sock_path)

def test():
	mac_addr_store.STORE_FILE='./known_macs'
	main.SURICATA_SOCKET='./pakond-dev-detect.sock'
	argv=["dev-detect", "br-lan"]
	p = Process(target = main.main, args=(argv,))
	p.start()
	fake_suricata(main.SURICATA_SOCKET)
	p.terminate() #from doc: "Terminate the process. On Unix this is done using the SIGTERM signal;"
	p.join()
	if os.path.exists(mac_addr_store.STORE_FILE):
		os.remove(mac_addr_store.STORE_FILE)
	if p.exitcode == -15: #exitcode==SIGTERM
		"""From doc:"""
		"""A negative value -N indicates that the child was terminated by signal N."""
		"""This means that the process lived until we terminated it -> that is expected"""
		return 0
	else:
		"""The process terminated itself, which shouldn't happen (it should try connecting forever)"""
		"""this isn't what we expect"""
		print("The process exited with unexpected exitcode {}".format(p.exitcode))
		return 1

if __name__ == "__main__":
	test()
